

#include <EEPROM.h>
#include <OneWire.h>
#include <DallasTemperature.h>


#include <Ethernet.h>
#include <Dns.h>
#include <SPI.h>
#define P_VALVULA  6 
#define P_MOTOR  7
#define LDR A0
//#define LED 13

const int oneWirePin = 2;
//const int addr_EEPROM = 0;
const int _nDevice = 10;

int _get = 34;
int _minlux = 800;

int counterA =0;
int finCicloA = 5;
int counterB =0;
int finCicloB = 1000;
int adressHistoric = 5;

boolean motor =LOW;
boolean paroForzado =LOW;

float h2o ;
float ambiente;
boolean releState;

String _json;

byte mac[] = {
  0x9A, 0xA2, 0xDA, 0x10, 0x1E, 0x00 + _nDevice
};
IPAddress ip(192, 168, 1, 230 + _nDevice);
IPAddress gateway(192,168,1, 1);
IPAddress dnServer(8,8,8,8);
IPAddress subnet(255, 255, 255, 0);
IPAddress ServiceHost;

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);
EthernetClient navigator;

 
OneWire oneWireBus(oneWirePin);
DallasTemperature sensor(&oneWireBus);

DNSClient ServiceDns;

//void clearEEPROM(){
//  for (int i = adressHistoric ; i < EEPROM.length() ; i++) {
//    if(EEPROM.read(i) != 0)                     //skip already "empty" addresses
//    {
//      EEPROM.write(i, 0);                       //write 0 to address i
//    }
//  }
//  Serial.print(EEPROM.length());
//  Serial.println("EEPROM erased");
  //adressHistoric = 1;                                  //reset address counter
//}
int readFromEEPROM (int _minlux){
    byte a = EEPROM.read(1);
    byte b = EEPROM.read(2);
    int _v = (a*256) + b; 
    if(_v!=0){
      Serial.println("read from epprom");
      Serial.println(a);
      Serial.println(b);
      Serial.println(_v);
      Serial.println("****************"); 
    }else{
      _v = _minlux;
    }
    return _v;
}
void saveMaxToEEPROM (String c){
  int _v = c.toInt();
  Serial.print("write to epprom "+c+"@");
  Serial.println(_v);
  if(_v>9 && _v<40){
    EEPROM.write(0,_v);
    _get = _v;
  }else{
    if(_v<2){
      paroForzado = (_v==0?LOW:HIGH);
      readTemperatura(0);
      _v = _get;
    }else{
       byte a = _v/256; 
       byte b = _v % 256; 
       EEPROM.write(1,a);
       EEPROM.write(2,b);
       Serial.println("write to epprom " + c);
       Serial.println(a);
       Serial.println(b);
       Serial.println((a*256)+b);
       Serial.println("****************");
       _minlux = _v;
    }
    
  }
}
int readMaxFromEEPROM (int _get){
    byte value_EEPROM = 0;
    value_EEPROM = EEPROM.read(0);    
    return ((value_EEPROM>0 && value_EEPROM<100)?value_EEPROM:_get);
}


void setup() {
    pinMode(P_VALVULA, OUTPUT);
    pinMode(P_MOTOR, OUTPUT); 
    

    //pinMode(LED, OUTPUT); 
    digitalWrite(P_VALVULA, LOW );
    digitalWrite(P_MOTOR, HIGH );

    Serial.begin(9600);
    // start the Ethernet connection and the server:
    Ethernet.begin(mac, ip, dnServer, gateway, subnet);
    server.begin();
    
    ServiceDns.begin(Ethernet.dnsServerIP());
    
    Serial.print("server is at ");
    Serial.println(Ethernet.localIP());
    Serial.print("DNS server is at ");
    Serial.println(Ethernet.dnsServerIP());
    //clearEEPROM();
    
    sensor.begin(); 
    _get = readMaxFromEEPROM(_get);
    _minlux = readFromEEPROM(_minlux);
    
    //digitalWrite(P_VALVULA, LOW );
    
    counterB=1;
    while(counterB>0){
      counterA++;
      delay(1);
      if(counterA>finCicloA){ 
        counterA=0;
        counterB++;     
        if(counterB>finCicloB){
            //digitalWrite(P_VALVULA, LOW );          
            delay(4000);            
            readTemperatura(1);
            counterB=0;
            finCicloA=54;
        }     
      }
    }
}


void loop() {

  // listen for incoming clients
  EthernetClient client = server.available();
 
  if (client) {
      webServer(client);
  }else{
    counterA++;
    delay(1);
    if(counterA>finCicloA){ 
      counterA=0;
      counterB++;     
      if(counterB>finCicloB){
          readTemperatura(counterB);
          counterB=0;
      }     
    }
  }
}


void readTemperatura(int n){
  
   sensor.requestTemperatures();
   
   h2o =  sensor.getTempCByIndex(0);
   ambiente =  sensor.getTempCByIndex(1);

   if(paroForzado == LOW && ambiente>10){  
     int lux = analogRead(LDR) > _minlux;
     if(motor == HIGH && lux){
       Serial.print(h2o<_get);
       Serial.print(ambiente>h2o);
       Serial.print((h2o<_get && ambiente>h2o));
       
       releState = (h2o<_get && ambiente>h2o);
       Serial.print(releState);
       digitalWrite(P_VALVULA, releState?HIGH:LOW );
       
     }else{
       if(lux){       
          motor = arrancaMotor(motor);       
       }else{
          motor = paraMotor(motor);
       }
     }
   }else{
     motor = paraMotor(motor);
   }
   
   String _act = action();
   _json = "{ \"_D\":" + String(_nDevice) + ",\"_h\":" + String(h2o) + ",\"_t\":" + String(ambiente) + ",\"Tmax\":" + String(_get) + ",\"_s\":" + String(releState)  + ",\"_m\":\"" + String(motor)  + "\",";
   _json += "\"_l\":" + String(analogRead(LDR)) + ",\"LMin\":" + String( _minlux )+ ",\"FSTOP\":" + String( paroForzado ) + ",\"_m\":\"" + StringMotor()  + "\",\"a\":\"" + action() + "}";

   Serial.println(_json.length());
   Serial.println("#########");
   
   if(n>0)
      sendDataWeb(_nDevice,h2o,ambiente,_get ,_minlux ,releState, motor ,paroForzado);
}
String StringMotor(){
  return (motor==HIGH?"ON":"off");
}
boolean arrancaMotor(boolean state){
  
  if(state==LOW){
     Serial.println("Arranca Motor");
     digitalWrite(P_VALVULA, LOW );
     delay(4000);
     digitalWrite(P_MOTOR, LOW );
     //digitalWrite(LED, HIGH );
  }else{
    Serial.println("Motor Arrancado");
  }
  return HIGH;
}
boolean paraMotor(boolean state){
  
  if(state==HIGH){
     Serial.println("para Motor");
     digitalWrite(P_VALVULA, LOW );
     delay(4000);
     digitalWrite(P_MOTOR, HIGH );
     releState =LOW;
  }else{
     Serial.println("Motor Parado");
  }
  return LOW;
}

String action(){
  return (ambiente>10?(motor==HIGH?(!releState?"Filtrando":"Calentando"):"Apagado"):"FRIO");
}

void webServer(EthernetClient client){
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean getParams = false;
    String Linea = "";
    
    boolean Fail = false;
    byte c[] = { 0, 0, 0, 0 ,0 };
    int _p = 0;
    
    _p=0;
    
    while (client.connected()) {
      if (client.available()) {
        char u = client.read();
        
        
        
        if((u==' ' || u == '\n') && getParams){
            getParams = false;                
        }else{
          if(u=='?' && _p==0){
             getParams  = true; 
          } else{
              if(getParams){
                c[_p] = u;
                _p++;
              }       
          }
        }
        
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (u == '\n' && currentLineIsBlank) {
            if( !Fail){             
              if(_p>0){
                saveMaxToEEPROM(c);
              }  
              readTemperatura(0);
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/json");
              client.println("Connection: close");  // the connection will be closed after completion of the response
              client.println();
              client.println(_json);
    
              Serial.println("###########");
            }else{
              Serial.println(".............");
            }
        
            break;
        }
        if (u == '\n') {
          // you're starting a new line
          Serial.print(Linea);
          if(!Fail)
            Fail = (Linea == "GET /favicon.ico HTTP/1.1");
          
          Serial.print(" Favicon=");
          Serial.println( Fail);
          Linea = "";
          currentLineIsBlank = true;
        } else if (u != '\r') {
          // you've gotten a character on the current line
          Linea = Linea + u;
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
}

void sendDataWeb(int _n,float _h2o,float _ambiente,int _max,int _lux, boolean _rele, boolean _motor ,boolean  pF){
    //data = "nDevice=" + String(_nDevice) + "&agua=" + String(h2o) + "&ambiente=" + String(ambiente)+ "&Tmax=" + String(_get)+ "&motor=" + String(motor) + "&paroForzado=" + String(paroForzado)+ "&lux=" + String(analogRead(LDR))+ "&umbral_lux=" + String( readFromEEPROM() )+ "&action=" + action();
 //return;
 
    Serial.print("POST ?_d="+String(_n) );
    Serial.print("&_h=" + String(_h2o) );
    Serial.print("&_t=" + String(_ambiente) ); 
    
    Serial.print("&_l=" + String(analogRead(LDR)) );
    Serial.print("&_s=" + String(_rele) );
    
    Serial.print("&_m=" + String(_motor) );
    
    Serial.print("&TMax=" + String(_max) );
    Serial.print("&LMin=" + String(_lux) ); 
    Serial.println("&FSTOP=" + String(pF) );
    //Serial.print("&STATE=" + action() );
    //Serial.println("&MOTOR=" + StringMotor());
       
  //Serial.println(_json);
  //Serial.println(_json.length());
  Serial.println("SEND TO -> ");
  
 if(ServiceDns.getHostByName("mipaloma.miraflores.com.es",ServiceHost) == 1) {
  Serial.println(ServiceHost);
  if (navigator.connect(ServiceHost,8090)) { // REPLACE WITH YOUR SERVER ADDRESS
    navigator.print("POST /?_d="+String(_n) );
    navigator.print("&_h=" + String(_h2o) );
    navigator.print("&_t=" + String(_ambiente) ); 
    
    navigator.print("&_l=" + String(analogRead(LDR)) );
    navigator.print("&_s=" + String(_rele) );
    
    navigator.print("&_m=" + String(_motor) );
    
    navigator.print("&TMax=" + String(_max) );
    navigator.print("&LMin=" + String(_lux) ); 
    navigator.print("&FSTOP=" + String(pF) );
    navigator.print("&STATE=") ;
    navigator.print( action() );
    navigator.print("&MOTOR=" );
    navigator.print(StringMotor());
    
    navigator.println(" HTTP/1.1"); 
    navigator.println("Host: mipaloma.miraflores.com.es"); // SERVER ADDRESS HERE TOO
    navigator.println("Content-Type: application/x-www-form-urlencoded"); 
    navigator.println("Content-Length: 2"); 
    //navigator.println(_json.length()); 
    navigator.println(); 
    navigator.println("{}");
    Serial.println(":-)");
    
  }else{
    Serial.println(":(");
  }
  
  if (navigator.connected()) { 
    navigator.stop();  // DISCONNECT FROM THE SERVER
  }
 }else{
  Serial.println("DNS FAIL");
  Serial.println(":("); 
 }
}



