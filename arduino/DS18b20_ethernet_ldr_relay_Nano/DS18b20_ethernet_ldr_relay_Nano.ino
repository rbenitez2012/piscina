#include <Dhcp.h>
#include <Dns.h>
#include <ethernet_comp.h>
#include <UIPEthernet.h>

//#include <UIPClient.h>
//#include <UIPEthernet.h>
//#include <UIPServer.h>
//#include <UIPUdp.h>



#include <EEPROM.h>
#include <OneWire.h>
#include <DallasTemperature.h>


//#include <Ethernet.h>
#define hallsensor 2
#define RELAY1  3 
#define RELAY2  4
#define LDR A0

//#define LED 13
DNSClient dnsx;

const int oneWirePin = 4;
//const int addr_EEPROM = 0;
const int _nDevice = 11;

int _get = 34;
int _minlux = 800;

int counterA =0;
int finCicloA = 5;
int counterB =0;
int finCicloB = 1000;
int adressHistoric = 5;

boolean motor =LOW;
boolean paroForzado =LOW;

float h2o ;
float ambiente;
boolean releState;

//String data ;
//String _json;

byte mac[] = {
  0x9A, 0xA2, 0xDA, 0x10, 0x1E, 0x00 + _nDevice
};





IPAddress ip(192, 168, 1, 230 + _nDevice);
IPAddress gateway(192,168,1, 1);
IPAddress dnServer(8,8,8,8);
IPAddress subnet(255, 255, 255, 0);
IPAddress ServiceHost;

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);
EthernetClient navigator;


 
OneWire oneWireBus(oneWirePin);
DallasTemperature sensor(&oneWireBus);

//void clearEEPROM(){
//  for (int i = adressHistoric ; i < EEPROM.length() ; i++) {
//    if(EEPROM.read(i) != 0)                     //skip already "empty" addresses
//    {
//      EEPROM.write(i, 0);                       //write 0 to address i
//    }
//  }
//  Serial.print(EEPROM.length());
//  Serial.println("EEPROM erased");
  //adressHistoric = 1;                                  //reset address counter
//}
volatile int NbTopsFan;
volatile int LitrosMinuto;
void rpm ()
{
NbTopsFan++;
}
int readFromEEPROM (int _minlux){
    byte a = EEPROM.read(1);
    byte b = EEPROM.read(2);
    int _v = (a*256) + b; 
    if(_v==0){
      _v = _minlux;
    }
    return _v;
}
void saveMaxToEEPROM (String c){
  int _v = c.toInt();
  Serial.print("write to epprom "+c+"@");
  Serial.println(_v);
  if(_v>9 && _v<40){
    EEPROM.write(0,_v);
    _get = _v;
  }else{
    if(_v<2){
      paroForzado = (_v==0?LOW:HIGH);
      readTemperatura(0);
      _v = _get;
    }else{
       byte a = _v/256; 
       byte b = _v % 256; 
       EEPROM.write(1,a);
       EEPROM.write(2,b);
       //Serial.println("write to epprom " + c);
       //Serial.println(a);
       //Serial.println(b);
       //Serial.println((a*256)+b);
       //Serial.println("****************");
       _minlux = _v;
    }
    
  }
}
int readMaxFromEEPROM (int _get){
    byte value_EEPROM = 0;
    value_EEPROM = EEPROM.read(0);    
    return ((value_EEPROM>0 && value_EEPROM<100)?value_EEPROM:_get);
}


void setup() {

    pinMode(hallsensor, INPUT);
    pinMode(RELAY1, OUTPUT);
    pinMode(RELAY2, OUTPUT); 

    attachInterrupt(digitalPinToInterrupt(2), rpm, RISING);
    Serial.begin(9600);

    //pinMode(LED, OUTPUT); 
    //digitalWrite(LED, HIGH );
    // start the Ethernet connection and the server:
    Ethernet.begin(mac, ip, dnServer, gateway, subnet);
    server.begin();
    
    dnsx.begin(Ethernet.dnsServerIP());
    
    Serial.print("server is at ");
    Serial.println(Ethernet.localIP());
    Serial.print("DNS server is at ");
    Serial.println(Ethernet.dnsServerIP());
    //clearEEPROM();
    
    sensor.begin(); 
    _get = readMaxFromEEPROM(_get);
    _minlux = readFromEEPROM(_minlux);
    
    digitalWrite(RELAY1, LOW );
    
    counterB=1;
    while(counterB>0){
      counterA++;
      delay(1);
      if(counterA>finCicloA){ 
        counterA=0;
        counterB++;     
        if(counterB>finCicloB){
            digitalWrite(RELAY1, HIGH );          
            delay(4000);            
            readTemperatura(1);
            counterB=0;
            finCicloA=1; //54;
        }     
      }
    }
}


void loop() {

  // listen for incoming clients
  EthernetClient client = server.available();
 
  if (client) {
      webServer(client);
  }else{
    counterA++;
    delay(1);
    if(counterA>finCicloA){ 
      counterA=0;
      counterB++;     
      if(counterB>finCicloB){
          readTemperatura(counterB);
          counterB=0;
      }     
    }
  }
}


void readTemperatura(int n){
    //Serial.println("%%%");
     _get = readMaxFromEEPROM(_get);
   
   int lux = analogRead(LDR) > readFromEEPROM(_minlux);  
   sensor.requestTemperatures();
   
   h2o =  sensor.getTempCByIndex(0);
   ambiente =  sensor.getTempCByIndex(1);
   
   NbTopsFan = 0;
   Serial.println("...");
   //sei();
   delay (1000);
   //cli();
   LitrosMinuto = (NbTopsFan * 60 / 5.5);
   Serial.println(LitrosMinuto);
   Serial.println(lux);
   
   if(paroForzado == LOW && ambiente>10){  
     
     
     if(motor == HIGH && lux){
       Serial.print(h2o<_get);
       Serial.print(ambiente>h2o);
       Serial.print((h2o<_get && ambiente+5>h2o));
       
       releState = (h2o<_get && ambiente>h2o);
       Serial.print(releState);
       digitalWrite(RELAY1, releState );
       
     }else{
       if(lux){       
          motor = arrancaMotor(motor);       
       }else{
          motor = paraMotor(motor);
       }
     }
   }else{
     motor = paraMotor(motor);
   }

   Serial.println("###");
   
   if(n>0)
      sendDataWeb(_nDevice,h2o,ambiente,_get ,releState, motor ,paroForzado,LitrosMinuto);
}

boolean arrancaMotor(boolean state){
  
  if(state==LOW){
     Serial.println("ON Mt");
     digitalWrite(RELAY1, LOW );
     delay(4000);
     digitalWrite(RELAY2, HIGH );
     //digitalWrite(LED, HIGH );
  }else{
    Serial.println("Mt ON");
  }
  return HIGH;
}
boolean paraMotor(boolean state){
  
  if(state==HIGH){
     Serial.println("off Mt");
     digitalWrite(RELAY1, LOW );
     delay(4000);
     digitalWrite(RELAY2, LOW );
     releState =LOW;
  }else{
     Serial.println("Mt");
  }
  return LOW;
}

String action( float _ambiente, boolean _motor, boolean _releState){
  return String(_ambiente>10?(_motor==HIGH?(!releState?"Filtrando":"Calentando"):"Apagado"):"FRIO");
}
String StringMotor(boolean _motor){
  return (_motor==HIGH?"ON":"off");
}

void webServer(EthernetClient client){
    Serial.println("new client");
    // an http request ends with a blank line


    getDataClient(client, false);

}
void getDataClient(EthernetClient client, boolean Final){

    boolean currentLineIsBlank = true;
    boolean getParams = false;
    String Linea = "";
    
    boolean Fail = false;
    byte c[] = { 0, 0, 0, 0 ,0 };
    int _p = 0;
    

  
      while (client.connected()) {
      if (client.available()) {
        char u = client.read();
        
             
        if((u==' ' || u == '\n') && getParams){
            getParams = false;                
        }else{
          if(u=='?' && _p==0){
             getParams  = true; 
          } else{
              if(getParams){
                c[_p] = u;
                _p++;
              }       
          }
        }
        
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (u == '\n' && currentLineIsBlank) {
            if( !Fail){
              Serial.print("response client "); 
              //Serial.print(String(c));            
              if(_p>0){
                saveMaxToEEPROM(c);
              }  
              if(Final){
                  readTemperatura(0);
                  client.println("HTTP/1.1 200 OK");
                  client.println("Content-Type: text/json");
                  client.println("Connection: close");  // the connection will be closed after completion of the response
                  client.println();
                  client.println("{ok:1}");
        
                  Serial.println("----");
                }else{
                  Serial.println("....");
                }
            }
            break;
        }
        if (u == '\n') {
          // you're starting a new line
          Serial.print(Linea);
          if(!Fail)
            Fail = (Linea == "GET /favicon.ico HTTP/1.1");
          
          Serial.print(" Favicon=");
          Serial.println( Fail);
          Linea = "";
          currentLineIsBlank = true;
        } else if (u != '\r') {
          // you've gotten a character on the current line
          Linea = Linea + u;
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
}
void sendDataWeb(int _nDevice,float h2o,float ambiente,int _max, boolean rele, boolean motor ,boolean  paroForzado,int LitrosMinuto){ //, String motor , String action){

   Serial.println("SEND TO -> ");

   if(dnsx.getHostByName("mipaloma.miraflores.com.es",ServiceHost) == 1) {
      Serial.println(ServiceHost);
      if (navigator.connect(ServiceHost,8090)) { // REPLACE WITH YOUR SERVER ADDRESS
          navigator.print("POST /sensor.html?");
          navigator.print("_d="+String(_nDevice) );
          
          navigator.print("&_h=" + String(h2o) );
          navigator.print("&_t=" + String(ambiente) ); 
          
          navigator.print("&_l=" + String(analogRead(LDR)) );
          navigator.print("&_s=" + String(rele) );
          
          navigator.print("&_m=" + motor );
          
          navigator.print("&TMax=" + String(_max) );
          navigator.print("&LMin=" + String(_minlux) ); 
          navigator.print("&FSTOP=" + String(paroForzado) );
          navigator.print("&CAUDAL=" + String(LitrosMinuto) );
          navigator.print("&STATE=") ;
          navigator.print( action( ambiente, motor, rele) );
          navigator.print("&MOTOR=" );
          navigator.print(StringMotor(motor));
          
          navigator.println(" HTTP/1.1"); 
          navigator.println("Host: mipaloma.miraflores.com.es"); // SERVER ADDRESS HERE TOO
          navigator.println("Content-Type: application/x-www-form-urlencoded"); 
          navigator.print("Content-Length: "); 
          navigator.println(2); 
          navigator.println(); 
          navigator.println("{}");
          
          Serial.println(":)");
      }else{
          Serial.println("FAIL");
          Serial.println(":(");
      }
      
      if (navigator.connected()) { 
        getDataClient(navigator, true);
      }
  
   }else{
      Serial.println("DNS FAIL");
      Serial.println(":("); 
   }
 
}



