$(document).ready(function() {
    var socket = io();
    var svgObjects = null;
     $('#rsr').draw('man', function(svgObjects){   
        socket.on('connect', function(){
            console.log('connect')
        });
        socket.on('hands', function(dataSensorArray){
            if(dataSensorArray.values.length>0)
                rePaint(svgObjects, dataSensorArray.values[10][dataSensorArray.values[10].length-1])
        })
        socket.on('lectura', function(dataSensor){
                rePaint(svgObjects,dataSensor)

        });
        socket.on('disconnect', function(){
            console.log('disconnect')
        });
        //var updatingChart = $(".updating-chart").peity("line", { width: 155, height:65 })
        //var thermometer = new Thermometer();
        //var container = document.getElementById('T_exterior');
        //thermometer.render(container, 0, 4, 40);
    
    })


    function rePaint(svgObjects,data){
        if(data._d==10){
            var hsl = hsl2rgb(30 + 240 * (30 - parseInt(data._h)) / 60 ,60,70) ;
            svgObjects.motor.silueta.attr({fill:data._m==1?'#3c9d00':'#de0000'})
        // svgObjects.motor.interior.attr({fill:data._m==1?'#3c9d00':'#de0000'})
            $('#waves').find('path').css({fill: 'rgb(' + hsl.r+',' + hsl.g+',' + hsl.b+')'})

        
            //svgObjects.agua.attr({fill: 'rgb(' + hsl.r+',' + hsl.g+',' + hsl.b+')'})

            var hsl = hsl2rgb(30 + 240 * (30 - parseInt(data._t)) / 60 ,60,70) ;
            svgObjects.panelSolar.attr({fill: data._s==0 ? '#3f5c6c' : 'rgb(' + hsl.r+',' + hsl.g+',' + hsl.b+')'})
            
            svgObjects.textos.T_placas.attr({text: data._t+" ºC"})
            svgObjects.textos.T_agua.attr({text: data._h+" ºC"})
            svgObjects.textos.Time.attr({text: data.date.d+"-"+ data.date.m+"-"+data.date.y+" "+data.date.h+":"+data.date.M+" "})
            svgObjects.textos.State.attr({text: data.STATE})

            svgObjects.img.icon.attr({'href':'/src?icon='+data.weather.icon}) //.removeClass('hidden')
            
            $('image').click(function(){ //})
            //svgObjects.img.icon.addEventListener('click', function() {
                console.log('hooray!');
                if( $('#PanelConfig').hasClass('hidden')){
                    $('#PanelConfig').removeClass('hidden')
                }else{
                    $('#PanelConfig').addClass('hidden')
                }
            });
            $('.TMax .slider').find('input').attr('data-value',data.TMax)
            $('.TLux .slider').find('input').attr('data-value',data.LMin)
           
            $('.Motor').find('input').prop('checked',data._m=="1")
            $('.Panel').find('input').prop('checked',data._s=="1")
            
            //$('#msg').html( JSON.stringify(data) )

            //var values = updatingChart.text().split(",")
            //if(values.length>640)
            //   values.shift()

            //values.push(data._h)
            //updatingChart.text(values.join(",")).change()
        }
        
    }        
    

    function hsl2rgb (h, s, l) {

        var r, g, b, m, c, x
    
        if (!isFinite(h)) h = 0
        if (!isFinite(s)) s = 0
        if (!isFinite(l)) l = 0
    
        h /= 60
        if (h < 0) h = 6 - (-h % 6)
        h %= 6
    
        s = Math.max(0, Math.min(1, s / 100))
        l = Math.max(0, Math.min(1, l / 100))
    
        c = (1 - Math.abs((2 * l) - 1)) * s
        x = c * (1 - Math.abs((h % 2) - 1))
    
        if (h < 1) {
            r = c
            g = x
            b = 0
        } else if (h < 2) {
            r = x
            g = c
            b = 0
        } else if (h < 3) {
            r = 0
            g = c
            b = x
        } else if (h < 4) {
            r = 0
            g = x
            b = c
        } else if (h < 5) {
            r = x
            g = 0
            b = c
        } else {
            r = c
            g = 0
            b = x
        }
    
        m = l - c / 2
        r = Math.round((r + m) * 255)
        g = Math.round((g + m) * 255)
        b = Math.round((b + m) * 255)
    
        return { r: r, g: g, b: b }
    
    }

})