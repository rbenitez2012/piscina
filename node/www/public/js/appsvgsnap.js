
(function( $ ) {
 
    $.fn.draw = function( action, callback ) {
 
        if ( action === "man") {
            //var _D = {w:420,h:796}
            var paper = Snap('#rsr').attr({
                //viewBox: "0 0 100% 100%",
                width: '50%',
                height: '50%'
              });; 
            //paper.setViewBox(0,0, _D.w,_D.h,true);
            //paper.text(10, 10, 'hola').data('id', 'test')
            //
            //paper.setSize('50%', '50%');
            //paper.canvas.style.backgroundColor = '#FFFEE9';
            //:label="panel_Solar"
            var tux = Snap.load("svg/panel_3.svg" , function ( svg ) {
                //debugger
                var panel = svg.select("[id='polygon213']")
                var motor = svg.select("[id='path10']")
                var agua = svg.select("[id='waves']")
                paper.append( svg );

                //var _g = paper.select("g")
               // var text = _g.text(0,0,'');
                //text.attr({ text: 'my new text'});
                //_g.add(text)
                //panel.attr({"fill":"red"})
                callback({agua:agua,motor:{silueta:motor},panelSolar:panel,                
                                textos:{
                                T_agua:app.textos(paper, 'T_agua', 45.491188, 22.307318, '', app.attrText('T_agua')),
                                T_placas:app.textos(paper, 'T_placas',275.491188, 25.307318, '', app.attrText('T_placas')),
                                Time:app.textos(paper, 'Time', 240.491188, 130.307318, '', app.attr( app.attrText('Time'),'font-size','0.8em')),
                                State:app.textos(paper, 'State', 135.491188, 125.307318, '', app.attrText('State'))
                            },
                            img: { icon:app.img(paper,'/src?icon=01d', 160, 1, 50,50) }
                        })
            } );
            return 


            var layer1 = paper.set();
            var svgObjects = { 
                man: {cabeza:app.man.cabeza(paper),cuerpo:app.man.cuerpo(paper)},
                agua: app.agua(paper),
                motor:{silueta: app.motor.silueta(paper) , interior:  app.motor.interior(paper)  },
                textos:{
                    T_agua:app.textos.T_agua(paper),
                    T_placas:app.textos.T_placas(paper),
                    Time:app.textos.Time(paper),
                    State:app.textos.State(paper)
                },
                panelSolar:{ paneles: app.panelSolar.paneles(paper) },
                img: { icon:app.img(paper,'/src?icon=01d', 80, 1, 50,50) }
            }
            layer1.attr({'id': 'layer1','name': 'layer1'});
            layer1.push( [svgObjects.cabeza, svgObjects.cuerpo, svgObjects.agua, svgObjects.motor.silueta ] ); 
            setTimeout(function(){
                svgObjects.agua.forEach(function(e){
                    e.attr({fill: '#9cf'})
                })
            },1000)
            return svgObjects
        }
 
        if ( action === "close" ) {
            // Close popup code.
        }
 
    };
    var app = {

        img: function(paper,url,x,y,w,h){
                return paper.image(url,x,y,w,h  )
                        .attr({ "clip-rect": x+","+y+","+w+","+h });
            
        },
        textos:function(paper,id,x,y,text, attr ){
            attr.id = id
            return paper.text(x, y, '').attr( attr ).data('id', id)
        
        }, 
        attrText:function(id){
            return {
                id: id,
                parent: 'layer1',
                "font-style": 'normal',
                "font-variant": 'normal',
                "font-weight": 'normal',
                "font-stretch": 'normal',
                "line-height": '125%',
                "font-family": 'ds_crystalregular',
                "letter-spacing": '0px',
                "word-spacing": '0px',
                fill: '#000000',
                "fill-opacity": '1',
                stroke: 'none',
                'stroke-width':'1',
                'stroke-opacity':'1',
                "stroke-width": '0.17',
                "stroke-linecap": 'butt',"stroke-linejoin": 'miter',
                "stroke-opacity": '1'}
        },attr:function(attr,key,value){
            attr[key] = value
            return attr
        }
        
    }
    
}( jQuery ));

