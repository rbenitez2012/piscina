
(function( $ ) {
 
    $.fn.draw = function( action ) {
 
        if ( action === "man") {
            var _D = {w:420,h:796}
            var paper = Raphael('rsr', _D.w, _D.h); 
            //paper.rect(0,0,_D.w,_D.h)
            paper.setViewBox(0,0, _D.w,_D.h,true);
            paper.setSize('100%', '100%');
            paper.canvas.style.backgroundColor = '#FFFEE9';
            var layer1 = paper.set();
            var svgObjects = { 
                man: {cabeza:app.man.cabeza(paper),cuerpo:app.man.cuerpo(paper)},
                agua: app.agua(paper),
                motor:{silueta: app.motor.silueta(paper) , interior:  app.motor.interior(paper)  },
                textos:{
                    T_agua:app.textos.T_agua(paper),
                    T_placas:app.textos.T_placas(paper),
                    Time:app.textos.Time(paper),
                    State:app.textos.State(paper)
                },
                panelSolar:{ paneles: app.panelSolar.paneles(paper) },
                img: { icon:app.img(paper,'/src?icon=01d', 80, 1, 50,50) }
            }
            layer1.attr({'id': 'layer1','name': 'layer1'});
            layer1.push( [svgObjects.cabeza, svgObjects.cuerpo, svgObjects.agua, svgObjects.motor.silueta ] ); 
            setTimeout(function(){
                svgObjects.agua.forEach(function(e){
                    e.attr({fill: '#9cf'})
                })
            },1000)
            return svgObjects
        }
 
        if ( action === "close" ) {
            // Close popup code.
        }
 
    };
    var app = {
        man:{
            cabeza: function(paper){
                return paper.circle(22, 13, 4).attr({id: 'cabeza',parent: 'layer1',"stroke-width": '0.11','stroke-opacity': '1','fill': '#000000'}).data('id', 'cabeza'); 
            },cuerpo:function(paper){
                return paper.path("m 19.96714,23.829177 1.20571,4.9027 c 1.99808,0.16015 3.85392,1.01144 5.28423,2.44187 0.27837,0.27837 0.65002,0.43174 1.0466,0.43174 0.39647,0 0.76812,-0.15326 1.04649,-0.43174 1.01006,-1.01006 2.23232,-1.73131 3.5667,-2.12513 l -2.08734,-8.48745 8.12338,-11.0282203 c 0.65254,-0.88588 0.46333,-2.13295 -0.42255,-2.78538 -0.88576,-0.65231 -2.13271,-0.46345 -2.78538,0.42243 L 26.73384,18.317357 c -0.11615,0.0232 -5.00839,1.2125 -5.29583,1.32382 L 11.37263,9.3945067 c -0.77088,-0.78466 -2.0319699,-0.79604 -2.8170899,-0.0252 -0.78489,0.7709903 -0.79615,2.0323103 -0.0251,2.8172003 z").attr({id: 'path69',parent: 'layer1',"stroke-width": '0.11','stroke-opacity': '1','fill': '#000000'}).data('id', 'cuerpo');
            } 
        },
        agua:function(paper){
                return [
                    paper.path("m 32.11687,29.048617 c -1.256491,1.25626 -3.300518,1.25626 -4.55701,0 -2.614773,-2.61489 -6.869638,-2.61512 -9.48464,0 -0.608658,0.60854 -1.417791,0.94377 -2.278621,0.94377 -0.860714,0 -1.669847,-0.33523 -2.278505,-0.94377 -0.680347,-0.680464 -1.783468,-0.680464 -2.463814,0 -0.680462,0.68034 -0.680462,1.78347 0,2.46382 2.615002,2.61511 6.869867,2.61488 9.48464,0 0.608658,-0.60855 1.41779,-0.94378 2.278505,-0.94378 0.86083,0 1.669962,0.33512 2.27862,0.94378 2.615003,2.61511 6.869867,2.61488 9.48464,0 0.608659,-0.60855 1.41779,-0.94378 2.278506,-0.94378 0.860715,0 1.669962,0.33512 2.27862,0.94378 1.266603,1.26671 2.95081,1.96429 4.74232,1.96429 1.79151,0 3.475604,-0.69758 4.74232,-1.96429 0.608658,-0.60855 1.417791,-0.94378 2.278621,-0.94378 0.860713,0 1.669846,0.33512 2.278504,0.94378 2.614774,2.61488 6.869639,2.61511 9.484641,0 0.680461,-0.68035 0.680461,-1.78348 0,-2.46382 -0.680347,-0.680464 -1.783469,-0.680464 -2.463813,0 -0.608658,0.60854 -1.417794,0.94377 -2.278507,0.94377 -0.860716,0 -1.669963,-0.33523 -2.278621,-0.94377 -2.615002,-2.61512 -6.869867,-2.614775 -9.484641,0 -0.608658,0.60866 -1.417788,0.94377 -2.278504,0.94377 -0.860716,0 -1.669849,-0.33511 -2.278507,-0.94377 -1.266717,-1.266719 -2.950924,-1.964299 -4.742319,-1.964299 -1.791395,0 -3.475602,0.69758 -4.742435,1.964299 z").attr({id: 'path73',parent: 'layer1',fill: '#d4a018',"stroke-width": '0.11','stroke-opacity': '1'}).data('id', 'path73'),
                    paper.path("m 60.200173,36.423857 c -0.608658,0.60855 -1.417791,0.94378 -2.278507,0.94378 -0.860716,0 -1.66996,-0.33512 -2.27862,-0.94378 -2.615002,-2.615 -6.869867,-2.61477 -9.484638,0 -0.608661,0.60866 -1.417791,0.94378 -2.278507,0.94378 -0.860716,0 -1.669846,-0.33512 -2.278505,-0.94378 -1.266719,-1.26671 -2.950927,-1.96429 -4.742321,-1.96429 -1.791279,0 -3.475487,0.69758 -4.742319,1.96429 -1.256493,1.25627 -3.300519,1.25627 -4.557011,0 -2.614773,-2.61488 -6.869637,-2.615 -9.48464,0 -0.608659,0.60855 -1.41779,0.94378 -2.27862,0.94378 -0.860716,0 -1.669847,-0.33512 -2.278506,-0.94378 -0.680346,-0.68046 -1.783468,-0.68046 -2.463814,0 -0.680461,0.68035 -0.680461,1.78347 0,2.46382 2.615002,2.615 6.869867,2.61489 9.484639,0 0.608659,-0.60854 1.417791,-0.94378 2.278506,-0.94378 0.86083,0 1.669962,0.33512 2.278621,0.94378 2.615002,2.615 6.869867,2.61489 9.484639,0 0.608658,-0.60854 1.417791,-0.94378 2.278505,-0.94378 0.860717,0 1.669964,0.33512 2.278622,0.94378 1.266603,1.26672 2.950808,1.9643 4.74232,1.9643 1.79151,0 3.475601,-0.69758 4.74232,-1.9643 0.608656,-0.60854 1.417789,-0.94378 2.278619,-0.94378 0.860716,0 1.669849,0.33512 2.278507,0.94378 2.614773,2.61489 6.869636,2.615 9.48464,0 0.680458,-0.68035 0.680458,-1.78347 0,-2.46382 -0.680463,-0.68034 -1.783583,-0.68034 -2.46393,0 z").attr({id: 'path75',parent: 'layer1',fill: '#d4a018',"stroke-width": '0.11','stroke-opacity': '1'}).data('id', 'path75'),
                    paper.path("m 60.200173,43.799217 c -0.608658,0.60855 -1.417791,0.94378 -2.278507,0.94378 -0.860716,0 -1.66996,-0.33512 -2.27862,-0.94378 -2.615002,-2.61511 -6.869867,-2.61477 -9.484638,0 -0.608661,0.60866 -1.417791,0.94378 -2.278507,0.94378 -0.860716,0 -1.669846,-0.33512 -2.278505,-0.94378 -1.266719,-1.26671 -2.950927,-1.96429 -4.742321,-1.96429 -1.791279,0 -3.475487,0.69758 -4.742319,1.96429 -1.256493,1.25627 -3.300519,1.25627 -4.557011,0 -2.614773,-2.61488 -6.869637,-2.61511 -9.48464,0 -0.608659,0.60855 -1.41779,0.94378 -2.27862,0.94378 -0.860716,0 -1.669847,-0.33512 -2.278506,-0.94378 -0.680346,-0.68046 -1.783468,-0.68046 -2.463814,0 -0.680461,0.68035 -0.680461,1.78347 0,2.46382 2.615002,2.61511 6.869867,2.61488 9.484639,0 0.608659,-0.60855 1.417791,-0.94378 2.278506,-0.94378 0.86083,0 1.669962,0.33512 2.278621,0.94378 2.615002,2.61511 6.869867,2.61488 9.484639,0 0.608658,-0.60855 1.417791,-0.94378 2.278505,-0.94378 0.860717,0 1.669964,0.33512 2.278622,0.94378 1.266603,1.26672 2.950808,1.9643 4.74232,1.9643 1.79151,0 3.475601,-0.69758 4.74232,-1.9643 0.608656,-0.60855 1.417789,-0.94378 2.278619,-0.94378 0.860716,0 1.669849,0.33512 2.278507,0.94378 2.614773,2.61488 6.869636,2.61511 9.48464,0 0.680458,-0.68035 0.680458,-1.78347 0,-2.46382 -0.680463,-0.68046 -1.783583,-0.68046 -2.46393,0 z").attr({id: 'path77',parent: 'layer1',fill: '#d4a018',"stroke-width": '0.11','stroke-opacity': '1'}).data('id', 'path77')
                ]
        },
        motor:{ 
            silueta:function(paper){
                return paper.path("m 402.65797,39.870336 h -26.90382 c -1.5769,0 -2.85567,-1.27848 -2.85567,-2.85567 v -5.91352 h -7.18315 c -0.0426,8.6e-4 -0.0854,8.6e-4 -0.12793,0 h -6.83247 v 5.91352 c 0,1.57719 -1.27849,2.85567 -2.85567,2.85567 h -26.90382 c -1.57719,0 -2.85567,-1.27848 -2.85567,-2.85567 v -6.13797 c -15.12419,-1.84048 -26.87898,-14.75924 -26.87898,-30.3720296 0,-15.6127974 11.75479,-28.5321264 26.87898,-30.3723194 v -3.73607 h -2.28968 c -1.57718,0 -2.85566,-1.278483 -2.85566,-2.855669 v -8.768899 c 0,-1.577185 1.27848,-2.855668 2.85566,-2.855668 h 37.1948 c 1.57718,0 2.85566,1.278483 2.85566,2.855668 v 8.769185 c 0,1.577186 -1.27848,2.855668 -2.85566,2.855668 h -2.28968 v 3.51133 h 6.83304 c 0.0423,-8.57e-4 0.0845,-8.57e-4 0.1268,0 h 36.80813 c 8.04099,0 14.70041,6.029172 15.69447,13.8043 h 6.04145 c 1.5769,0 2.85567,1.278483 2.85567,2.855664 v 28.00554 c 0,1.57719 -1.27877,2.85567 -2.85567,2.85567 h -6.05887 c -0.91752,6.72539 -6.07772,12.11917 -12.68631,13.3888 v 6.1968 c -2.8e-4,1.57719 -1.27876,2.85567 -2.85595,2.85567 z m -24.04815,-5.71134 h 21.19248 v -3.05785 h -21.19248 z m -46.75872,0 h 21.19249 v -3.05785 H 331.8511 Z m 43.90305,-8.76918 h 26.76846 c 5.57541,0 10.11135,-4.53595 10.11135,-10.11135 v -29.54789 c 0,-5.575404 -4.53594,-10.111633 -10.11135,-10.111633 h -32.646 c 2.89708,7.89478 4.36261,16.2461834 4.36261,24.8854334 0,8.63926 -1.46582,16.9906596 -4.36261,24.8854396 z m -15.63107,0 h 3.63355 c 3.16751,-7.83795 4.77153,-16.1996396 4.77153,-24.8854396 0,-8.6858 -1.60402,-17.0474804 -4.77153,-24.8854334 h -3.63355 c 2.89707,7.89478 4.3626,16.2458934 4.3626,24.8854334 0,8.63954 -1.46581,16.9909396 -4.3626,24.8854396 z m -30.26552,0 h 24.14525 c 3.16722,-7.83767 4.77153,-16.1993496 4.77153,-24.8854396 0,-8.68608 -1.60431,-17.0477664 -4.77153,-24.8854334 h -24.14525 c -13.72177,0 -24.88543,11.163663 -24.88543,24.8854334 0,13.7217696 11.16366,24.8854396 24.88543,24.8854396 z m 88.48803,-13.67237 h 3.05756 v -22.2942 h -3.05756 z m -86.49449,-41.80984 h 21.19249 v -3.511329 H 331.8511 Z m -5.14562,-9.222664 h 31.48345 v -3.057564 h -31.48345 z").attr({id: 'path10',parent: 'layer1',fill: '#de0000',"fill-opacity": '1',"stroke-width": '0.29','stroke-opacity': '1'}).transform("m-0.16479898,0,0,0.16479898,143.17675,45.070753").data('id', 'path10');
            },
            interior:function(paper){
                var g65 = paper.set();
                g65.attr({'id': 'g65','style': 'fill:#de0000;fill-opacity:1','parent': 'layer1','name': 'g65'});
                g65.transform("m-0.16479898,0,0,0.16479898,143.17675,45.070753"); 
                return g65
            }
        },
        panelSolar:{ 
            paneles : function(paper){
                var scretch = {
                    pie1: paper.path("M 291.31,388.414 317.793,485.517 194.207,485.517 220.69,388.414 z").attr({id: 'polygon191',parent: 'layer1',fill: '#bdc3c7','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon191'),
                    pie2:paper.path("m 128.64278,52.161258 h -20.81732 c -0.23449,0 -0.45234,-0.121422 -0.5756,-0.320954 -0.12326,-0.199533 -0.13453,-0.448585 -0.0296,-0.658389 l 0.67671,-1.353342 c 0.1146,-0.229275 0.34893,-0.374076 0.60527,-0.374076 h 19.46398 c 0.25633,0 0.49059,0.144801 0.60526,0.374076 l 0.67671,1.353342 c 0.10487,0.209728 0.0937,0.458856 -0.0296,0.658389 -0.1235,0.199532 -0.34135,0.320954 -0.57584,0.320954 z").attr({id: 'path193',parent: 'layer1',fill: '#3f5c6c',"stroke-width": '0.08','stroke-opacity': '1'}).data('id', 'path193'), 
                    placas: paper.path("M 495.316,291.31 489.843,273.655 475.895,229.517 470.422,211.862 456.474,167.724 451.001,150.069 434.317,97.103 79.448,97.103 61.263,150.069 58.262,158.897 55.526,167.724 41.578,211.862 36.105,229.517 22.157,273.655 16.684,291.31 0,344.276 0,397.241 512,397.241 512,344.276 z").attr({id: 'placas',parent: 'layer1',fill: '#192b91',"fill-opacity": '1','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'placas'),
                    marco:paper.rect(98.610466, 39.30439, 39.247311, 4.0601034).attr({id: 'rect215',y: '39.30439',x: '98.610466',parent: 'layer1',fill: '#3f5c6c',"stroke-width": '0.08','stroke-opacity': '1'}).data('id', 'rect215') 
                }
                scretch.polygon217 = paper.path("M 451.001,150.069 456.474,167.724 55.526,167.724 58.262,158.897 61.263,150.069 z").attr({id: 'polygon217',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon217'); 
                scretch.polygon219 = paper.path("M 470.422,211.862 475.895,229.517 36.105,229.517 41.578,211.862 z").attr({id: 'polygon219',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon219'); 
                scretch.polygon221 = paper.path("M 489.843,273.655 495.316,291.31 16.684,291.31 22.157,273.655 z").attr({id: 'polygon221',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon221'); 
                scretch.polygon223 = paper.path("M 166.047,150.069 164.017,160.574 162.781,167.724 155.277,211.862 152.188,229.517 144.684,273.655 141.683,291.31 132.59,344.276 114.67,344.276 123.763,291.31 126.764,273.655 134.268,229.517 137.357,211.862 144.861,167.724 146.626,157.396 148.039,150.069 158.455,97.103 176.463,97.103 z").attr({id: 'polygon223',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon223'); 
                scretch.polygon225 = paper.path("M 380.116,344.276 370.847,291.31 367.757,273.655 359.989,229.517 356.899,211.862 349.219,167.724 347.895,160.397 346.13,150.069 336.861,97.103 354.781,97.103 363.961,150.069 366.963,167.724 374.643,211.862 377.644,229.517 385.324,273.655 386.56,280.982 388.414,291.31 397.771,344.276 z").attr({id: 'polygon225',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'polygon225'); 
                scretch.rect227 = paper.rect(247.172, 97.102997, 17.655001, 247.172).attr({id: 'rect227',y: '97.102997',x: '247.172',parent: 'layer1',fill: '#e6e7e8','stroke-width': '0','stroke-opacity': '1'}).transform("m0.0766549,0,0,0.0766549,98.610467,12.913949").data('id', 'rect227'); 

                //var g229 = paper.set();
                return scretch
            }
        },
        img: function(paper,url,x,y,w,h){
                return paper.image(url,x,y,w,h  )
                        .attr({ "clip-rect": x+","+y+","+w+","+h });
            
        },
        textos:{
            T_agua: function(paper){
                return paper.text(45.491188, 22.307318, '').attr({
                    id: 'T_agua',
                    parent: 'layer1',
                    "font-style": 'normal',
                    "font-variant": 'normal',
                    "font-weight": 'normal',
                    "font-stretch": 'normal',
                    "font-size": '6.69645739px',
                    "line-height": '125%',
                    "font-family": 'ds_crystalregular',
                    "-inkscape-font-specification": 'ds_crystalregular',
                    "letter-spacing": '0px',
                    "word-spacing": '0px',
                    fill: '#000000',
                    "fill-opacity": '1',
                    stroke: 'none',
                    'stroke-width':'1',
                    'stroke-opacity':'1',
                    "stroke-width": '0.17',
                    "stroke-linecap": 'butt',"stroke-linejoin": 'miter',
                    "stroke-opacity": '1'}).data('id', 'T_agua')
            },
            T_placas: function(paper){
                return paper.text(125.491188, 15.307318, '').attr({
                    id: 'T_agua',
                    parent: 'layer1',
                    "font-style": 'normal',
                    "font-variant": 'normal',
                    "font-weight": 'normal',
                    "font-stretch": 'normal',
                    "font-size": '6.69645739px',
                    "line-height": '125%',
                    "font-family": 'ds_crystalregular',
                    "-inkscape-font-specification": 'ds_crystalregular',
                    "letter-spacing": '0px',
                    "word-spacing": '0px',
                    fill: '#000000',
                    "fill-opacity": '1',
                    stroke: 'none',
                    'stroke-width':'1',
                    'stroke-opacity':'1',
                    "stroke-width": '0.17',
                    "stroke-linecap": 'butt',"stroke-linejoin": 'miter',
                    "stroke-opacity": '1'}).data('id', 'T_placas')
            },
            Time: function(paper){
                return paper.text(125.491188, 2.307318, 'Sin Lectura').attr({
                    id: 'T_agua',
                    parent: 'layer1',
                    "font-style": 'normal',
                    "font-variant": 'normal',
                    "font-weight": 'normal',
                    "font-stretch": 'normal',
                    "font-size": '6.69645739px',
                    "line-height": '125%',
                    "font-family": 'ds_crystalregular',
                    "-inkscape-font-specification": 'ds_crystalregular',
                    "letter-spacing": '0px',
                    "word-spacing": '0px',
                    fill: '#000000',
                    "fill-opacity": '1',
                    stroke: 'none',
                    'stroke-width':'1',
                    'stroke-opacity':'1',
                    "stroke-width": '0.17',
                    "stroke-linecap": 'butt',"stroke-linejoin": 'miter',
                    "stroke-opacity": '1'}).data('id', 'T_placas')
            },
            State: function(paper){
                return paper.text(83.491188, 55.307318, '').attr({
                    id: 'State',
                    parent: 'layer1',
                    "font-style": 'normal',
                    "font-variant": 'normal',
                    "font-weight": 'normal',
                    "font-stretch": 'normal',
                    "font-size": '6.69645739px',
                    "line-height": '125%',
                    "font-family": 'ds_crystalregular',
                    "-inkscape-font-specification": 'ds_crystalregular',
                    "letter-spacing": '0px',
                    "word-spacing": '0px',
                    fill: '#000000',
                    "fill-opacity": '1',
                    stroke: 'none',
                    'stroke-width':'1',
                    'stroke-opacity':'1',
                    "stroke-width": '0.17',
                    "stroke-linecap": 'butt',"stroke-linejoin": 'miter',
                    "stroke-opacity": '1'}).data('id', 'State')
            }
        }
    }
    
}( jQuery ));

