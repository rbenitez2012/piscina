'use strict';

var express = require('express');
var router = express.Router();
var request = require('request');

//debugger

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

router.get('/src', function (req, res) {
    var requestSettings = {
        url: 'http://openweathermap.org/img/w/'+req.query.icon+'.png',
        method: 'GET',
        encoding: null
    };

    request(requestSettings, function(error, response, body) {
        res.set('Content-Type', 'image/png');
        res.send(body);
    });
});




module.exports = router;
