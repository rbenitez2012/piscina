
//var node_env = process.env.NODE_ENV || 'development';
//var envJSON = require('./env.variables.json');
//var mysql      = require('mysql');
//var connection = mysql.createConnection({
//  host     : envJSON[node_env].host, //'mariadb.bbdd.ovh',
//  user     : envJSON[node_env].user,
//  password : envJSON[node_env].password,
//  database : envJSON[node_env].database
//});
var fs = require('fs');
var express = require('express');
var path = require('path');
var _ = require('lodash')
var http = require('http');
var request = require('request');
//var cookieParser = require('cookie-parser');
//var bodyParser = require('body-parser');

var routes = require('./www/routes/index');

var lastdata = []


var app =   express() //.createServer(credentials);

// view engine setup
app.set('views', path.join(__dirname, 'www/views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(bodyParser.json({limit: '50mb'}));
//app.use(bodyParser.urlencoded({ limit: '50mb',extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'www/public')));

app.use('/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var httpServer = http.createServer(app);
var io = require('socket.io')(httpServer);


httpServer.listen(80);
io.on('connection', function(socket){
    console.log('connection socket io established');
    app.socket = socket
    socket.emit('hands', {values:lastdata} )

    socket.on('disconnect', function(){
        console.log('user disconnected');
        app.socket = null
    });
});

var SensorServer = http.createServer(function (req, res) {
    console.log("client come in...") 

    if(req.method=='POST') {
        var body='';
        req.on('data', function (data) {
            body +=data;
        });
        req.on('end',function(){
            //console.log(body);
            var date =new Date()
            
            res.end();
            //console.log(req.url.split('&'))
            //var params = getDateTime(JSON.parse(body))
           // _.each(   body.split('&') , function(e){
           //     var _p = e.split("=")
           //     params[_p[0]] = _p[1]
           // })
            //console.log(params) ;
            
            var _s = {}
            var _p = req.url.split('?')
            if(_p.length>1){
                _p = _p[1].split('&')
                _.each(_p,function(i,v){
                    var _t = i.split("=")
                    if(_t.length==2)
                        _s[_t[0]] = _t[1]
                })
            }
            if(_s!={} && _s._d==10){
                _s = getDateTime(_s)
                request('https://api.openweathermap.org/data/2.5/weather?lat=40.8138&lon=-3.7668&appid=d03faec224430ba5bd447731b949a467&lang=ES', { json: true },function(err, res, body){
                    console.log(err);    
                    console.log(body);
                  
                        _s.weather = { 
                            descripcion:body.weather? body.weather[0].description:'',
                            icon:body.weather? body.weather[0].icon:''
                        }

                    _s.date.dt = body.weather.dt
                    _s.date.sunrise = body.sys.sunrise
                    _s.date.sunset = body.sys.sunset

                    if(lastdata[_s._d] == null)
                        lastdata[_s._d] = []

                    if(lastdata[_s._d].length>1440)
                        lastdata[_s._d].shift()

                    lastdata[_s._d].push(_s)

                    if( app.socket != null){                    
                        app.socket.emit('lectura',_s)
                    }

                })
                
            }
            console.log(_s) ;            
            //.connect(function(err){
                //connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
                //    connection.end();
                //    if (error) 
                //        console.log(error);
                    //console.log('The solution is: ', results[0].solution);
                //});
            //    connection.end();
                
            //});       
        });
    }



}).listen(8090);



function getDateTime(params) {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    if(params==null){
        params = {}
    }
    params.date = {
        y:year ,
        m : month,
        d:day,
        h:hour,
        M:min,
        s:sec,
    }
 

    
    return params;

}